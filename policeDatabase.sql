CREATE DATABASE spring_police;
CREATE USER rool WITH password '0816';
GRANT ALL ON DATABASE spring_police TO rool;
CREATE SCHEMA this_is_the_police;

CREATE SEQUENCE dept_count;
CREATE TABLE this_is_the_police.departments (
  dept_id INTEGER PRIMARY KEY DEFAULT NEXTVAL('dept_count'),
  title CHAR(64) NOT NULL,
  spokesman CHAR(64) NOT NULL,
  contact CHAR(64),
  phone CHAR(64)  
);

insert into this_is_the_police.departments (title, spokesman, contact, phone) values
('Уголовный розыск', 'полковник Исаев М.М.', 'ул. Пушкина, дом 25', '263-26-52')
insert into this_is_the_police.departments (title, spokesman, contact, phone) values
('Следственный комитет', 'полковник Петров И.В.', 'ул. Пушкина, дом 25', '263-26-53')

CREATE SEQUENCE duty_count;
CREATE TABLE this_is_the_police.duties (
  duty_id INTEGER PRIMARY KEY DEFAULT NEXTVAL('duty_count'),
  report CHAR(2000) NOT NULL,
  rout CHAR(2000) NOT NULL
);

CREATE SEQUENCE human_count;
CREATE TABLE this_is_the_police.humans (
  employer_id INTEGER PRIMARY KEY DEFAULT NEXTVAL('human_count'),
  name CHAR(64) NOT NULL,
  rank CHAR(64) NOT NULL,
  characters CHAR(4000),
  dept_id   INTEGER REFERENCES this_is_the_police.departments(dept_id),
  list_id INTEGER,
  birthday CHAR(64),
  login    CHAR(64),
  password CHAR(64)
);

insert into this_is_the_police.humans (name, rank, characters, dept_id, list_id, birthday)
values ('Серов Сергей Павлович', 'Капитан', 'Туп, жаден, прожорлив, ленив, трусоват, надменен. Характер отсутствует. Не женат.', '1', '1', '20-09-1990')
insert into this_is_the_police.humans (name, rank, characters, dept_id, list_id, birthday)
values ('Иванов Анатолий Потапович', 'Майор', 'Очень хороший и веселый человек. Характер общительный. Не женат.', '1', '1', '03-10-1985')


CREATE SEQUENCE dlc;
CREATE TABLE this_is_the_police.duty_list (
    d_id         INTEGER PRIMARY KEY DEFAULT NEXTVAL('dlc'),
    emp_id       INTEGER REFERENCES this_is_the_police.humans(employer_id),
    id_dqd       INTEGER REFERENCES this_is_the_police.duties(duty_id)
);

CREATE SEQUENCE case_count;
CREATE TABLE this_is_the_police.cases (
  case_id       INTEGER PRIMARY KEY DEFAULT NEXTVAL('case_count'),
  id_dept       INTEGER REFERENCES this_is_the_police.departments(dept_id),
  figurants_id  INTEGER,
  description   CHAR(2000) NOT NULL,
  emp_list      INTEGER,
  name          CHAR(2000),
  data          CHAR(2000),
  city          CHAR(2000),
  isend         CHAR(2000),
  status        CHAR(2000)
);

insert into this_is_the_police.cases (id_dept, figurants_id, description, emp_list, name, data, city, isend, status)
values ('2', '2', 'Интересное дело с неявным убийцей', '2', 'Убийство в восточном экспрессе', '12-12-2018', 'Самара', 'Да', 'В архиве')

CREATE SEQUENCE link_count;
CREATE TABLE this_is_the_police.case_link (
  link_id       INTEGER PRIMARY KEY DEFAULT NEXTVAL('link_count'),
  empl_id       INTEGER REFERENCES this_is_the_police.humans(employer_id),
  case_id       INTEGER REFERENCES this_is_the_police.cases(case_id),
  charge        CHAR(2000) NOT NULL
);

CREATE SEQUENCE figurants_count;
CREATE TABLE this_is_the_police.figurants (
  figurant_id       INTEGER PRIMARY KEY DEFAULT NEXTVAL('figurants_count'),
  name              CHAR(64) NOT NULL,
  status            CHAR(64) NOT NULL,
  characters        CHAR(4000)
);

insert into this_is_the_police.figurants (name, status, photo, characters)
values('Дагомирова Наталья Александровна', 'подозреваемая', 'пожилая княгиня, чей платок был найден на месте преступления')

CREATE TABLE this_is_the_police.figurants_link (
  fig_id        INTEGER REFERENCES this_is_the_police.figurants(figurant_id),
  case_id       INTEGER REFERENCES this_is_the_police.cases(case_id)
);


