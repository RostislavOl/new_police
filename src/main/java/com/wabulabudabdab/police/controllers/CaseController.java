/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.controllers;

import com.wabulabudabdab.police.entities.Cases;
import com.wabulabudabdab.police.service.GenericTypeService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/case")
public class CaseController {
    
    @Autowired
    private GenericTypeService service;
    
    @RequestMapping(method = RequestMethod.GET, path = "/cases/")
    public ResponseEntity<?> getAllCases() {
        ArrayList<Cases> depts = new ArrayList<>();
        depts.addAll(service.findAll(Cases.class));
        return ResponseEntity.status(HttpStatus.OK).body(depts);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/cases/{caseID}")
    public ResponseEntity<?> getCase(@PathVariable Integer caseID) {
        Cases dept = service.getById(Cases.class, caseID);
        return ResponseEntity.status(HttpStatus.OK).body(dept);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/cases/{caseID}")
    public ResponseEntity<?> deleteCase(@PathVariable Integer caseID) {
        service.delete(Cases.class, caseID);
        return ResponseEntity.status(HttpStatus.OK).body("deleted");
    }

    @RequestMapping(method = RequestMethod.POST, path = "/cases/")
    public ResponseEntity<?> createCase(@RequestBody Cases c) {
        service.add(c);
        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/cases")
    public ResponseEntity<?> updateCase(@RequestBody Cases c) {
        service.update(c);
        return ResponseEntity.status(HttpStatus.OK).body("updated");
    }
    
}
