/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.controllers;

import com.wabulabudabdab.police.entities.Departments;
import com.wabulabudabdab.police.service.GenericTypeService;
import java.util.ArrayList;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/dept")
public class DepartmentController {
    
    @Autowired
    private GenericTypeService service;
    
    @RequestMapping(method = RequestMethod.GET, path = "/departments/")
    public ResponseEntity<?> getAllDepts() {
        ArrayList<Departments> depts = new ArrayList<>();
        depts.addAll(service.findAll(Departments.class));
        return ResponseEntity.status(HttpStatus.OK).body(depts);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/departments/{deptID}")
    public ResponseEntity<?> getDept(@PathVariable Integer deptID) {
        Departments dept = service.getById(Departments.class, deptID);
        return ResponseEntity.status(HttpStatus.OK).body(dept);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/departments/{deptID}")
    public ResponseEntity<?> deleteDept(@PathVariable Integer deptID) {
        service.delete(Departments.class, deptID);
        return ResponseEntity.status(HttpStatus.OK).body("deleted");
    }

    @RequestMapping(method = RequestMethod.POST, path = "/departments/")
    public ResponseEntity<?> createDept(@RequestBody Departments dept) {
        System.out.println(dept);
        LoggerFactory.getLogger(DepartmentController.class).info("Hi, im here!!!!");
        service.add(dept);
        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/departments")
    public ResponseEntity<?> update(@RequestBody Departments dept) {
        service.update(dept);
        return ResponseEntity.status(HttpStatus.OK).body("updated");
    }
    
}
