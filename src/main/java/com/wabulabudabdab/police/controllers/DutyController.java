/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.controllers;

import com.wabulabudabdab.police.entities.Duty;
import com.wabulabudabdab.police.service.GenericTypeService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/dy")
public class DutyController {
    
    @Autowired
    private GenericTypeService service;
    
    @RequestMapping(method = RequestMethod.GET, path = "/duties/")
    public ResponseEntity<?> getAllDuties() {
        ArrayList<Duty> depts = new ArrayList<>();
        depts.addAll(service.findAll(Duty.class));
        return ResponseEntity.status(HttpStatus.OK).body(depts);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/duties/{dutyID}")
    public ResponseEntity<?> getDuty(@PathVariable Integer dutyID) {
        Duty dept = service.getById(Duty.class, dutyID);
        return ResponseEntity.status(HttpStatus.OK).body(dept);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/duties/{dutyID}")
    public ResponseEntity<?> deleteDuty(@PathVariable Integer dutyID) {
        service.delete(Duty.class, dutyID);
        return ResponseEntity.status(HttpStatus.OK).body("deleted");
    }

    @RequestMapping(method = RequestMethod.POST, path = "/duties/")
    public ResponseEntity<?> createDuty(@RequestBody Duty duty) {
        service.add(duty);
        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/duties")
    public ResponseEntity<?> updateDuty(@RequestBody Duty duty) {
        service.update(duty);
        return ResponseEntity.status(HttpStatus.OK).body("updated");
    }
    
}
