/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.controllers;

import com.wabulabudabdab.police.entities.DutyList;
import com.wabulabudabdab.police.service.GenericTypeService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/dl")
public class DutyListController {
    
    @Autowired
    private GenericTypeService service;
    
    @RequestMapping(method = RequestMethod.GET, path = "/dls/")
    public ResponseEntity<?> getAllCases() {
        ArrayList<DutyList> dl = new ArrayList<>();
        dl.addAll(service.findAll(DutyList.class));
        return ResponseEntity.status(HttpStatus.OK).body(dl);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/dls/{dlID}")
    public ResponseEntity<?> getCase(@PathVariable Integer dlID) {
        DutyList dl = service.getById(DutyList.class, dlID);
        return ResponseEntity.status(HttpStatus.OK).body(dl);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/dls/{dlID}")
    public ResponseEntity<?> deleteCase(@PathVariable Integer dlID) {
        service.delete(DutyList.class, dlID);
        return ResponseEntity.status(HttpStatus.OK).body("deleted");
    }

    @RequestMapping(method = RequestMethod.POST, path = "/dls/")
    public ResponseEntity<?> createCase(@RequestBody DutyList dl) {
        service.add(dl);
        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/dls")
    public ResponseEntity<?> updateCase(@RequestBody DutyList dl) {
        service.update(dl);
        return ResponseEntity.status(HttpStatus.OK).body("updated");
    }
    
}
