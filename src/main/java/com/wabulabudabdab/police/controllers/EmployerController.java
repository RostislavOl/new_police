/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.controllers;

import com.wabulabudabdab.police.entities.Employers;
import com.wabulabudabdab.police.service.GenericTypeService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/emp")
public class EmployerController {
    
    @Autowired
    private GenericTypeService service;
    
    @RequestMapping(method = RequestMethod.GET, path = "/employers/")
    public ResponseEntity<?> getAllCases() {
        ArrayList<Employers> emp = new ArrayList<>();
        emp.addAll(service.findAll(Employers.class));
        return ResponseEntity.status(HttpStatus.OK).body(emp);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/employers/{empID}")
    public ResponseEntity<?> getCase(@PathVariable Integer empID) {
        Employers emp = service.getById(Employers.class, empID);
        return ResponseEntity.status(HttpStatus.OK).body(emp);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/employers/{empID}")
    public ResponseEntity<?> deleteCase(@PathVariable Integer empID) {
        service.delete(Employers.class, empID);
        return ResponseEntity.status(HttpStatus.OK).body("deleted");
    }

    @RequestMapping(method = RequestMethod.POST, path = "/employers/")
    public ResponseEntity<?> createCase(@RequestBody Employers emp) {
        service.add(emp);
        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/employers")
    public ResponseEntity<?> updateCase(@RequestBody Employers emp) {
        service.update(emp);
        return ResponseEntity.status(HttpStatus.OK).body("updated");
    }
    
}
