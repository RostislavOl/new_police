/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.controllers;

import com.wabulabudabdab.police.entities.Figurants;
import com.wabulabudabdab.police.service.GenericTypeService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/f")
public class FigurantController {
    
    @Autowired
    private GenericTypeService service;
    
    @RequestMapping(method = RequestMethod.GET, path = "/figurants/")
    public ResponseEntity<?> getAllCases() {
        ArrayList<Figurants> emp = new ArrayList<>();
        emp.addAll(service.findAll(Figurants.class));
        return ResponseEntity.status(HttpStatus.OK).body(emp);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/figurants/{figID}")
    public ResponseEntity<?> getCase(@PathVariable Integer figID) {
        Figurants emp = service.getById(Figurants.class, figID);
        return ResponseEntity.status(HttpStatus.OK).body(emp);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/figurants/{figID}")
    public ResponseEntity<?> deleteCase(@PathVariable Integer figID) {
        service.delete(Figurants.class, figID);
        return ResponseEntity.status(HttpStatus.OK).body("deleted");
    }

    @RequestMapping(method = RequestMethod.POST, path = "/figurants/")
    public ResponseEntity<?> createCase(@RequestBody Figurants fig) {
        service.add(fig);
        return ResponseEntity.status(HttpStatus.OK).body("created");
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/figurants")
    public ResponseEntity<?> updateCase(@RequestBody Figurants fig) {
        service.update(fig);
        return ResponseEntity.status(HttpStatus.OK).body("updated");
    }
    
}
