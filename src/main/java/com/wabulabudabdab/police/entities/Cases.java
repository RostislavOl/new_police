/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cases")
public class Cases {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "case_id")
    private Integer id;
    
    @Column(name = "id_dept")
    private Integer dept_id;
    
    @Column(name = "figurants_id")
    private Integer fig_list_id;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "emp_list")
    private Integer emp_list;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "data")
    private String data;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "isend")
    private String isEnd;

    @Column(name = "status")
    private String status;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDept_id() {
        return dept_id;
    }

    public void setDept_id(Integer dept_id) {
        this.dept_id = dept_id;
    }

    public Integer getFig_list_id() {
        return fig_list_id;
    }

    public void setFig_list_id(Integer fig_list_id) {
        this.fig_list_id = fig_list_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEmp_list() {
        return emp_list;
    }

    public void setEmp_list(Integer emp_list) {
        this.emp_list = emp_list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(String isEnd) {
        this.isEnd = isEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
