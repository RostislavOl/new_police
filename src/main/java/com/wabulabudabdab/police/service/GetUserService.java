/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wabulabudabdab.police.service;

import com.wabulabudabdab.police.entities.Employers;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author root
 */
public class GetUserService {
    
    @Autowired
    private EntityManagerFactory emf;
    
    @Autowired
    private GenericTypeService service;
    
    public Employers getUserByName(String userName) {
        System.out.println(userName);
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("select u from humans u where u.name=:name", Employers.class);
        query.setParameter("name", userName);
        List<Employers> enabledUsers = query.getResultList();
        em.getTransaction().commit();
        em.close();
        Employers activeUser = null;
        if (!enabledUsers.isEmpty()) {
            activeUser = enabledUsers.get(0);
        }
        return activeUser;
    }
    
}
