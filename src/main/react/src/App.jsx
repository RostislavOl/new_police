import React, { Component } from 'react';
import {
    BrowserRouter,
    Link,
    Route,
    Switch
} from 'react-router-dom';



import Dejr from './dejr.jsx';
import Delo from './delo.jsx';
import Figa  from './figa.jsx';
import Otdl from './otdl.jsx';
import Sotr  from './sotr.jsx';
import './data/data.css';

import {Navbar, Nav, NavItem, Grid, Row, MenuItem, NavDropdown} from 'react-bootstrap';

class App extends Component {
  render() {
    return (
      <div className="App">
      <Grid>
  <Row className="show-grid">

        <BrowserRouter basename={process.env.REACT_APP_ROUTER_BASE || ''}>
          <div>


          <Navbar inverse collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <a >ГУ МВД Руси</a>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>

              <NavItem eventKey={1}>
                     <Link className="ahref" to="/">Отдел</Link>
                   </NavItem>
                   <NavItem eventKey={2} >
                     <Link className="ahref" to="/sotr">Сотрудники</Link>
                   </NavItem>

                   <NavItem eventKey={5} >
                    <Link className="ahref" to="/dejr">Дежурства</Link>
                   </NavItem>

                   <NavItem eventKey={4}>
                     <Link className="ahref" to="/figa">Фигуранты</Link>
                   </NavItem>
                   <NavItem eventKey={5} >
                    <Link className="ahref" to="/delo">Дела</Link>
                   </NavItem>

                   <NavItem pullRight className="tratata" eventKey={6} >
                    Здравствуйте, Олешко Р.А. <Link className="ahref" to="http://localhost:8084/"> выход</Link>
                   </NavItem>
              </Nav>

            </Navbar.Collapse>
          </Navbar>

<div className="Ros">

            <Switch>
              <Route path="/dejr" component={Dejr}/>
              <Route path="/sotr" component={Sotr}/>
              <Route path="/delo" component={Delo}/>
              <Route path="/figa" component={Figa}/>
              <Route path="/" component={Otdl}/>
            </Switch>
            </div>
          </div>
        </BrowserRouter>

        </Row>
</Grid>
      </div>
    );
  }
}

export default App;
