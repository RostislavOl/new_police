import React from 'react';

class Dejr extends React.Component {
  render() {
    return (
      <div id="Calen" className="home">


      <div class="calendar">


  <ul class="weekdays">
    <li>
      <p title="S">Воскресенье</p>
    </li>
    <li>
      <p title="M">Понедельник</p>
    </li>
    <li>
      <p title="T">Вторник</p>
    </li>
    <li>
      <p title="W">Среда</p>
    </li>
    <li>
      <p title="T">Четверг</p>
    </li>
    <li>
      <p title="F">Пятница</p>
    </li>
    <li>
      <p title="S">Суббота</p>
    </li>
  </ul>

  <ul class="day-grid">
    <li class="month=prev">29</li>
    <li class="month=prev">30</li>
    <li class="month=prev">31</li>
    <li>1<br />Иванов</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
    <li>5</li>
    <li>6</li>
    <li>7</li>
    <li>8</li>
    <li>9</li>
    <li>10</li>
    <li>11</li>
    <li>12</li> 
    <li>13</li>
    <li>14</li>
    <li>15</li>
    <li>16</li>
    <li>17</li>
    <li>18</li>
    <li>19</li>
    <li>20</li>
    <li>21</li>
    <li>22</li>
    <li>23</li>
    <li>24</li>
    <li>25</li>
    <li>26</li>
    <li>27</li>
    <li>28</li>
    <li>29</li>
    <li>30</li>
    <li class="month-next">1</li>
    <li class="month-next">2</li>
  </ul>

</div>

      </div>
    );
  }
}

export default Dejr;
