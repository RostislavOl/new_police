import React from 'react';
import {Navbar, Nav, Table, Col,FormGroup, Tab, ControlLabel, Form,FormControl, Button, Modal, NavItem, Grid, Row, MenuItem, NavDropdown} from 'react-bootstrap';
import $ from 'jquery';
import {
    BrowserRouter,
    Link,
    Route,
    Switch
} from 'react-router-dom';

function requestGet(url) {
  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'GET',

      url: url,
      dataType: 'json',
      success: resolve,
      error: reject
    });
  });
}

class Delo extends React.Component {

  constructor(props, context) {
  super(props, context);
  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleHide = this.handleHide.bind(this);

  this.handleShow = this.handleShow.bind(this);
  this.handleShowDetail = this.handleShowDetail.bind(this);
  this.handleShowEdit = this.handleShowEdit.bind(this);
  this.handleHide = this.handleHide.bind(this);
  this.handleSelect = this.handleSelect.bind(this);

  this.state = {
    show: false,
    showdetail: false,
    showedit: false,
    key: 1,
    datas: [],
    country: []
  };
}

componentDidMount() {
  Promise.all(
    [ requestGet('http://localhost:8084/case/cases/'),
    requestGet('http://localhost:8084/f/figurants/') ]).then(([ datas, country ]) =>
    this.setState({ datas, country }));

}

handleShow() {
  this.setState({ show: true });
}
handleShowDetail() {
  this.setState({ showdetail: true });
}
handleShowEdit() {
  this.setState({ showedit: true });
}

handleHide() {
  this.setState({ show: false,showedit: false, showdetail: false});
}

  handleSelect(key) {
    console.log(`selected ${key}`);
  this.setState({ key });

  }

  handleChange(event) {

    this.setState({id: event.target.id});
    this.setState({dept_id: event.target.dept_id});
    this.setState({fig_list_id: event.target.fig_list_id});
    this.setState({description: event.target.description});
    this.setState({name: event.target.name});
    this.setState({data: event.target.data});
    this.setState({city: event.target.city});
    this.setState({isEnd: event.target.isEnd});
      this.setState({status: event.target.status});
  }

  handleSubmit(event){

    event.preventDefault();
      console.log(this.state.dept_id)
      console.log(this.state.fig_list_id)
      console.log(this.state.description)
      console.log(this.state.name)
      console.log(this.state.data)
      console.log(this.state.city)
      console.log(this.state.isEnd)
      console.log(this.state.status)
      this.setState({ show: false });
    $.ajax ({
         url:"http://localhost:8084/case/cases/",
        type: "POST",
        data: JSON.stringify({

          id:"",
          dept_id:this.state.dept_id,
          fig_list_id:this.state.fig_list_id,
          description:this.state.description,
          name:this.state.name,
          data:this.state.data,
          city:this.state.city,
          isEnd:this.state.isEnd,
          status:this.state.status
        }),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(res) {
            console.log(res);
            this.setState({ show: false });
            console.log("Added");
        }.bind(this)
    });
  };

  DropData(key) {

      console.log(`на удаление ${key}`);
      $(`#left-tabs-example-tab-${key}`).hide();

      $.ajax ({

           url:`http://localhost:8084/case/cases/${key}`,
          type: "DELETE",

          dataType: "json",
          contentType: "application/json; charset=utf-8",
          success: function(res) {

          }.bind(this)
      });

  }

  render() {
    return (
      <div className="home">
      <Modal
        {...this.props}
        show={this.state.show}
        onHide={this.handleHide}
        dialogClassName="custom-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">
            Добавить новое дело
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>

          <Row className="kar">
          <form onSubmit={this.handleSubmit}>




            <FormGroup  className="hidden" controlId="case_id_dela">
              <ControlLabel>id дела</ControlLabel>
              <FormControl value={this.state.id} onChange={(ev)=>this.setState({id:ev.target.value})} type="text" placeholder="Невводимый элемент" />
            </FormGroup>
            <FormGroup controlId="case_id_dept">
              <ControlLabel>id департамента</ControlLabel>
              <FormControl value={this.state.dept_id} onChange={(ev)=>this.setState({dept_id:ev.target.value})} type="text" placeholder="ID департамента" />
            </FormGroup>

            <FormGroup controlId="case_city">
             <ControlLabel>Выбор фигуранта</ControlLabel>
             <FormControl onChange={(ev)=>this.setState({fig_list_id:ev.target.value})}  componentClass="select" placeholder="select">

             <option value="">Выбрать...</option>
             <option value="0">Отсутствует </option>
             {this.state.country.map(function (item, key) {
                 return (
                     <option value={item.id}>{item.name} </option>
                   )
               })}


             </FormControl>
           </FormGroup>

            <FormGroup controlId="case_names">
              <ControlLabel>Наименование</ControlLabel>
              <FormControl value={this.state.name} onChange={(ev)=>this.setState({name:ev.target.value})} type="text" placeholder="Введите заголовок дела" />
            </FormGroup>

            <FormGroup controlId="case_names">
              <ControlLabel>Описание</ControlLabel>
              <FormControl value={this.state.description} onChange={(ev)=>this.setState({description:ev.target.value})} type="text" placeholder="Расскажите, как это было" />
            </FormGroup>
            <FormGroup controlId="case_data">
              <ControlLabel>Дата</ControlLabel>
              <FormControl value={this.state.data} onChange={(ev)=>this.setState({data:ev.target.value})} type="date" placeholder="Когда завели дело?" />
            </FormGroup>

            <FormGroup controlId="case_city">
             <ControlLabel>В каком городе</ControlLabel>
             <FormControl onChange={(ev)=>this.setState({city:ev.target.value})} componentClass="select" placeholder="select">
               <option value="0">Выбрать...</option>
               <option value="Самара">Самара</option>
               <option value="Москва">Москва</option>
               <option value="Санкт-Питербург">Санкт-Питербург</option>
               <option value="Саратов">Саратов</option>
               <option value="Воркута">Воркута</option>
               <option value="Саранск">Саранск</option>
               <option value="Сочи">Сочи</option>
               <option value="Анапа">Анапа</option>
             </FormControl>
           </FormGroup>

           <FormGroup controlId="case_isEnd">
            <ControlLabel>Завершенность</ControlLabel>
            <FormControl onChange={(ev)=>this.setState({isEnd:ev.target.value})} componentClass="select" placeholder="select">
               <option value="0">Выбрать...</option>
              <option value="Закрыто">Закрыто</option>
              <option value="Открыто">Открыто</option>

            </FormControl>
          </FormGroup>


          <FormGroup controlId="case_status">
           <ControlLabel>Статус</ControlLabel>
           <FormControl onChange={(ev)=>this.setState({status:ev.target.value})}  componentClass="select" placeholder="select">
               <option value="0">Выбрать...</option>
             <option value="В архиве">В архиве</option>
             <option value="Оцифровано, но недоступно">Оцифровано, но недоступно</option>
             <option value="Не оцифровано">Не оцифровано</option>
           </FormControl>
         </FormGroup>


                </form>
          </Row>
        </Modal.Body>
        <Modal.Footer>
        <input className="btn btn-primary" type="submit" onClick={this.handleSubmit} value="Отправить" />
          <Button onClick={this.handleHide}>UnОтправить</Button>
        </Modal.Footer>
      </Modal>

      <Nav onSelect={key => this.handleSelect(key)}>
             <MenuItem className="roso2"  onClick={this.handleShow} >Добавить дело</MenuItem>
   <MenuItem className="roso2"   onClick={() => this.DropData(this.state.key)} >Удалить</MenuItem>
           </Nav>


   <Row>


   <Tab.Container
       activeKey={this.state.key}
       onSelect={this.handleSelect}
       id="left-tabs-example"
       defaultActiveKey="1">
<Nav className="ororo">
       {this.state.datas.map(function (item, key) {
           return (

               <NavItem className="nopoint"  key={key} eventKey={item.id}>




                <Col xs="12" sm="12" className="paddingfull10">
                <Col className="borders" xs="12" sm="12">



                <Col xs="1" id={item.id} className="nopadding"><b>Дело №:</ b> {item.id} </Col>
                <Col xs="2" id={item.dept_id} className="nopadding" ><b>Департамент №:</ b> {item.dept_id}</Col>
                <Col xs="3" id={item.name} className="nopadding" ><b>Наименование:</ b> {item.name}</Col>
                 <Col xs="2" id={item.data} className="nopadding" ><b>Заведено:</ b> {item.data} </Col>
                   <Col xs="2" id={item.city} className="nopadding" ><b>Город:</ b> {item.city}</Col>
               <Col xs="2" id={item.status} className="nopadding" ><b>Статус:</ b> {item.status} </Col>
                 <Col xs="12" id={item.description} className="nopadding" ><b>Описание дела:<br /></ b> {item.description} </Col>
                </Col>
                </Col>


               </NavItem>
             )
         })}

  </Nav>
</Tab.Container>



   </Row>


      </div>
    );
  }
}

export default Delo;
