import React from 'react';
import {Navbar, Nav, Col, Tab, FormGroup,ControlLabel, FormControl, Button, Table, Modal, NavItem, Grid, Row, MenuItem, NavDropdown} from 'react-bootstrap';
import $ from 'jquery';
import {
    BrowserRouter,
    Link,
    Route,
    Switch
} from 'react-router-dom';

function requestGet(url) {
  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      success: resolve,
      error: reject
    });
  });
}


class Figa extends React.Component {

  constructor(props, context) {
  super(props, context);

this.DropData = this.DropData.bind(this);
  this.handleSelect = this.handleSelect.bind(this);
  this.handleShow = this.handleShow.bind(this);
  this.handleShowDetail = this.handleShowDetail.bind(this);
  this.handleHide = this.handleHide.bind(this);
this.handleSubmit = this.handleSubmit.bind(this);



  this.state = {
    show: false,
    key: 1,
    showdetail: false,
    showedit: false,
    datas: []

  };
}


componentDidMount() {
  Promise.all(
    [ requestGet('http://localhost:8084/f/figurants/') ]).then(([ datas ]) =>
    this.setState({ datas}));

}

handleSelect(key) {
  console.log(`selected ${key}`);
this.setState({ key });

}

handleShow() {
  this.setState({ show: true });
}
handleShowDetail() {
  this.setState({ showdetail: true });
}


handleHide() {
  this.setState({ show: false, showdetail: false});
}

handleSelect(key) {
  console.log(`selected ${key}`);
this.setState({ key });

}





handleGetModal(key) {
   console.log("good")
   console.log(`принят в модал ${key}`);
   $.ajax({
        type: 'GET',
        url: `http://localhost:8084/f/figurants/${key}`,
        dataType: 'json',
        success: function(res) {



            console.log(res.id)
  console.log(res.name)
    console.log(res.status)
      console.log(res.characteristic)






             $('.figa_id').val(res.id);
             $('.figa_name').val(res.name);
             $('.figa_status').val(res.status);
             $('.figa_characteristic').val(res.characteristic);


        }
      });
 }

UpdateData(key){
console.log(`отправлен ${key}`);
  $.ajax ({

       url:`http://localhost:8084/f/figurants/`,
      type: "PUT",
      data: JSON.stringify({
        id:$('.figa_id').val(),
        name:$('.figa_name').val(),
              rank:$('.figa_rank').val(),
        characteristic:$('.figa_characteristic').val(),


      }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function(res) {

      }.bind(this)
  });
};


handleChange(event) {

  this.setState({id: event.target.id});
  this.setState({name: event.target.name});
    this.setState({status: event.target.status});
  this.setState({characteristic: event.target.characteristic});


}

handleSubmit(event){

  event.preventDefault();
console.log(this.state.id)
 console.log(this.state.name)
 console.log(this.state.status)
 console.log(this.state.characteristic)

    this.setState({ show: false });
  $.ajax ({
       url:"http://localhost:8084/f/figurants/",
      type: "POST",
      data: JSON.stringify({

        id:"",
        name:this.state.name,
        status:this.state.status,
        characteristic:this.state.characteristic,


      }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function(res) {
          console.log(res);
          this.setState({ show: false });
          console.log("Added");
      }.bind(this)
  });
};


DropData(key) {

      console.log(`на удаление ${this.state.key}`);


    $.ajax ({

         url:`http://localhost:8084/f/figurants/${this.state.key}`,
        type: "DELETE",

        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(res) {

        }.bind(this)
    });

}

  render() {
    return (
      <div className="home">



      <Modal
        {...this.props}
        show={this.state.show}
        onHide={this.handleHide}
        dialogClassName="custom-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">
           Новый фигурант
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>

        <form onSubmit={this.handleSubmit}>




                        <FormGroup  className="hidden"  controlId="case_id_dela">
                          <ControlLabel>id фигуранта</ControlLabel>
                          <FormControl className="figa_id" value={this.state.id} onChange={(ev)=>this.setState({id:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>

                        <FormGroup controlId="case_id_dela">
                          <ControlLabel>ФИО фигуранта</ControlLabel>
                          <FormControl className="figa_name" value={this.state.name} onChange={(ev)=>this.setState({name:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>

                        <FormGroup controlId="case_id_dela">
                          <ControlLabel>Статус</ControlLabel>
                          <FormControl className="figa_status" value={this.state.status} onChange={(ev)=>this.setState({status:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>

                        <FormGroup controlId="case_id_dela">
                          <ControlLabel>Описательная характеристика</ControlLabel>
                          <FormControl className="figa_characteristic" value={this.state.characteristic} onChange={(ev)=>this.setState({characteristic:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>







              </form>

        </Modal.Body>
        <Modal.Footer>
        <input className="btn btn-primary" type="submit" onClick={this.handleSubmit} value="Отправить" />



          <Button onClick={this.handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>



      <Modal
        {...this.props}
        show={this.state.showdetail}
        onHide={this.handleHide}
        onEntered={() => this.handleGetModal(this.state.key)}
        dialogClassName="custom-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">
            Управлять сотрудником
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>


                                <FormGroup  className="hidden"  controlId="case_id_dela">
                                  <ControlLabel>id фигуранта</ControlLabel>
                                  <FormControl className="figa_id" value={this.state.id} onChange={(ev)=>this.setState({id:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>ФИО фигуранта</ControlLabel>
                                  <FormControl className="figa_name" value={this.state.name} onChange={(ev)=>this.setState({name:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>Статус</ControlLabel>
                                  <FormControl className="figa_status" value={this.state.status} onChange={(ev)=>this.setState({status:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>Описательная характеристика</ControlLabel>
                                  <FormControl className="figa_characteristic" value={this.state.characteristic} onChange={(ev)=>this.setState({characteristic:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>


        </Modal.Body>
        <Modal.Footer>
                            <input className="btn btn-primary" type="submit" onClick={this.UpdateData} value="Отправить" />
          <Button onClick={this.handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>









      <Nav onSelect={key => this.handleSelect(key)}>
             <MenuItem className="roso2"  onClick={this.handleShow} eventKey={this.state.key}>Добавить фигуранта</MenuItem>
             <MenuItem className="roso2"  onClick={this.handleShowDetail} eventKey={this.state.key}>Управлять фигурантом</MenuItem>
             <MenuItem className="roso2"   onClick={this.DropData}  eventKey={this.state.key}>Удалить фигуранта</MenuItem>
           </Nav>











      <Tab.Container
      activeKey={this.state.key}
      onSelect={this.handleSelect}
      >
 <Row className="clearfix">
   <Col sm={12}>

   {this.state.datas.map(function (item, key) {
       return (



     <Nav bsStyle="pills" eventKey={item.id}stacked>


       <NavItem eventKey={item.id}>
       <p className="ppad">
       <Col xs="2" className="nopadding"><b>Номер: </ b> <span>{item.id}</span></Col>
       <Col xs="4" className="nopadding" ><b>ФИО: </ b> <span>{item.name}</span></Col>
      <Col xs="3" className="nopadding" ><b>статус: </ b> <span>{item.status}</span></Col>
       <Col xs="3" className="nopadding" ><b>Дата рождения: </ b><span>{item.birthday}</span></Col>

     </p>
       </NavItem>










     </Nav>

   )
})}

   </Col>
   <Col sm={8}>

   </Col>
 </Row>
</Tab.Container>

      </div>
    );
  }
}

export default Figa;
