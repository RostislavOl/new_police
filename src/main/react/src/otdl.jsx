import React from 'react';
import {Navbar, Nav, Col, Tab, FormGroup,ControlLabel, FormControl, Button, Table, Modal, NavItem, Grid, Row, MenuItem, NavDropdown} from 'react-bootstrap';
import $ from 'jquery';
import {
    BrowserRouter,
    Link,
    Route,
    Switch
} from 'react-router-dom';

function requestGet(url) {
  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      success: resolve,
      error: reject
    });
  });
}


class Otdl extends React.Component {

  constructor(props, context) {
  super(props, context);
  this.handleSubmit = this.handleSubmit.bind(this);
  this.handleGetModal = this.handleGetModal.bind(this);

  this.handleSelect = this.handleSelect.bind(this);
  this.handleShow = this.handleShow.bind(this);
  this.handleShowDetail = this.handleShowDetail.bind(this);
  this.handleShowEdit = this.handleShowEdit.bind(this);
  this.handleHide = this.handleHide.bind(this);
  this.DropData = this.DropData.bind(this);


  this.state = {
    show: false,
    showdetail: false,
    showedit: false,
    key: 1,
    depart: [],
    casess: []
  };
}

componentDidMount() {
  Promise.all(
    [ requestGet('http://localhost:8084/dept/departments/') ]).then(([ depart ]) =>
    this.setState({ depart}));

}




handleChange(event) {

  this.setState({id: event.target.id});
  this.setState({title: event.target.title});
    this.setState({contact: event.target.contact});
  this.setState({spokesman: event.target.spokesman});
  this.setState({phone: event.target.phone});

}


handleGetModal(key) {
   console.log("good")
   console.log(`принят в модал ${key}`);
   $.ajax({
        type: 'GET',
        url: `http://localhost:8084/dept/departments/${key}`,
        dataType: 'json',
        success: function(res) {



            console.log(res.id)
  console.log(res.title)
    console.log(res.spokesman)
      console.log(res.contact)
        console.log(res.phone)

             $('.dept_id').val(res.id);
             $('.dept_tittle').val(res.title);
             $('.dept_nach').val(res.spokesman);
             $('.dept_addr').val(res.contact);
             $('.dept_tel').val(res.phone);


        }
      });
 }

UpdateData(key){
console.log(`отправлен ${key}`);
  $.ajax ({

       url:`http://localhost:8084/dept/departments/`,
      type: "PUT",
      data: JSON.stringify({
        id:$('.dept_id').val(),
        title:$('.dept_tittle').val(),
        spokesman:$('.dept_nach').val(),
        contact:$('.dept_addr').val(),
        phone:$('.dept_tel').val()

      }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function(res) {

      }.bind(this)
  });
};



handleSubmit(event){
  this.setState({ show: false });
  event.preventDefault();
    console.log(this.state.id)
    console.log(this.state.title)
    console.log(this.state.spokesman)
    console.log(this.state.contact)
        console.log(this.state.phone)

    this.setState({ show: false });
  $.ajax ({
       url:"http://localhost:8084/dept/departments/",
      type: "POST",
      data: JSON.stringify({

        id:"",
        title:this.state.title,
        spokesman:this.state.spokesman,
        contact:this.state.contact,
        phone:this.state.phone,

      }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function(res) {
          console.log(res);
          this.setState({ show: false });
          console.log("Added");
      }.bind(this)
  });
};


DropData(key) {

    console.log(`на удаление ${this.state.key}`);
    $(`#Boo-${this.state.key}`).hide();


    $.ajax ({

         url:`http://localhost:8084/dept/departments/${this.state.key}`,
        type: "DELETE",

        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(res) {

        }.bind(this)
    });

}

handleShow() {
  this.setState({ show: true });
}
handleShowDetail() {
  this.setState({ showdetail: true });
}
handleShowEdit() {
  this.setState({ showedit: true });
}

handleHide() {
  this.setState({ show: false,showedit: false, showdetail: false});
}

handleSelect(key) {
  console.log(`selected ${key}`);
this.setState({ key });

}

  render() {
    return (
      <div className="home">





              <Modal
                {...this.props}
                show={this.state.show}
                onHide={this.handleHide}
                dialogClassName="custom-modal"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title-lg">
                    Добавить отдел
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                <form onSubmit={this.handleSubmit}>



                <FormGroup  className="hidden"  controlId="case_id_dela1">
                  <ControlLabel>id отдела</ControlLabel>
                  <FormControl value={this.state.id} onChange={(ev)=>this.setState({id:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>Название отдела</ControlLabel>
                  <FormControl value={this.state.title} onChange={(ev)=>this.setState({title:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>Начальник отдела</ControlLabel>
                  <FormControl value={this.state.spokesman} onChange={(ev)=>this.setState({spokesman:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>Адрес местонахождения</ControlLabel>
                  <FormControl value={this.state.contact} onChange={(ev)=>this.setState({contact:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>телефон справочной</ControlLabel>
                  <FormControl value={this.state.phone} onChange={(ev)=>this.setState({phone:ev.target.value})} type="text" placeholder="" />
                </FormGroup>




                      </form>


                </Modal.Body>
                <Modal.Footer>
                <input className="btn btn-primary" type="submit" onClick={this.handleSubmit} value="Отправить" />
                  <Button onClick={this.handleHide}>UnОтправить</Button>
                </Modal.Footer>
              </Modal>



              <Modal
                {...this.props}
                show={this.state.showdetail}
                onHide={this.handleHide}
                onEntered={() => this.handleGetModal(this.state.key)}
                container={this}
                dialogClassName="custom-modal"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title-lg">
                    Подробнее об отделе
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                  <p>
                    инпуты
                  </p>
                </Modal.Body>
                <Modal.Footer>
                  <Button onClick={this.handleHide}>Close</Button>
                </Modal.Footer>
              </Modal>



              <Modal
                {...this.props}
                show={this.state.showedit}
                onHide={this.handleHide}
                onEntered={() => this.handleGetModal(this.state.key)}
                container={this}
                dialogClassName="custom-modal"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title-lg">
              Редактирование
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>

                <form onSubmit={this.handleSubmit}>




                <FormGroup  className="hidden"  controlId="case_id_dela">
                  <ControlLabel>id отдела</ControlLabel>
                  <FormControl className="dept_id" value={this.state.id} onChange={(ev)=>this.setState({id:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>Название отдела</ControlLabel>
                  <FormControl className="dept_tittle" value={this.state.name} onChange={(ev)=>this.setState({title:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>Начальник отдела</ControlLabel>
                  <FormControl className="dept_nach" value={this.state.spokesman} onChange={(ev)=>this.setState({spokesman:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>Адрес местонахождения</ControlLabel>
                  <FormControl className="dept_addr" value={this.state.contact} onChange={(ev)=>this.setState({contact:ev.target.value})} type="text" placeholder="" />
                </FormGroup>

                <FormGroup controlId="case_id_dela">
                  <ControlLabel>телефон справочной</ControlLabel>
                  <FormControl className="dept_tel" value={this.state.phone} onChange={(ev)=>this.setState({phone:ev.target.value})} type="text" placeholder="" />
                </FormGroup>




                      </form>
                </Modal.Body>
                <Modal.Footer>
                                <input className="btn btn-primary" type="submit" onClick={this.UpdateData} value="Отправить" />
                  <Button onClick={this.handleHide}>Close</Button>
                </Modal.Footer>
              </Modal>



        <Row className="padding10">


        <Nav onSelect={key => this.handleSelect(key)}>
               <MenuItem className="roso2"  onClick={this.handleShow} eventKey={this.state.key}>Добавить отдел</MenuItem>
 
               <MenuItem className="roso2"  onClick={this.handleShowEdit} eventKey={this.state.key}>Редактировать</MenuItem>
               <MenuItem className="roso2"   onClick={this.DropData}  eventKey={this.state.key}>Удалить</MenuItem>
             </Nav>


             <Tab.Container
                 activeKey={this.state.key}
                 onSelect={this.handleSelect}
                 id="left-tabs-example"
                 defaultActiveKey="1">
          <Nav className="ororo">
             {this.state.depart.map(function (item, key) {
                 return (



                     <NavItem className="itemo" id={`Boo-${item.id}`} key={key} eventKey={item.id}>


                            <Col

                            xs="12" sm="6" className="paddingfull10">
                            <Col className="borders" xs="12" sm="12">
                     <Col xs="3" className="nopadding"><b>Отдел</ b> № {item.id}</Col>
                      <Col xs="9" className="nopadding centered">{item.title}</Col>



                       <Col xs="12" className="innerline"></Col>

                      <Col xs="12" className="nopadding"><b>Телефон:</ b> {item.phone}</Col>
                      <Col xs="12" className="nopadding" ><b>Начальник:</ b> {item.spokesman}</Col>
                      <Col xs="12" className="nopadding" ><b>Адрес:</ b> {item.contact}</Col>
                            </Col>
                            </Col>


                    </NavItem>
                   )
               })}


               </Nav>
             </Tab.Container>




       </Row>

      </div>
    );
  }
}

export default Otdl;
