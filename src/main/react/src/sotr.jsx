import React from 'react';
import {Navbar, Nav, Col, Tab, FormGroup,ControlLabel, FormControl, Button, Table, Modal, NavItem, Grid, Row, MenuItem, NavDropdown} from 'react-bootstrap';
import $ from 'jquery';
import {
    BrowserRouter,
    Link,
    Route,
    Switch
} from 'react-router-dom';

function requestGet(url) {
  return new Promise((resolve, reject) => {
    $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      success: resolve,
      error: reject
    });
  });
}


class Sotr extends React.Component {

  constructor(props, context) {
  super(props, context);

this.DropData = this.DropData.bind(this);
  this.handleSelect = this.handleSelect.bind(this);
  this.handleShow = this.handleShow.bind(this);
  this.handleShowDetail = this.handleShowDetail.bind(this);
  this.handleHide = this.handleHide.bind(this);
this.handleSubmit = this.handleSubmit.bind(this);



  this.state = {
    show: false,
    key: 1,
    showdetail: false,
    showedit: false,
    datas: [],
    delas: [],
    departs: []

  };
}


componentDidMount() {
  Promise.all(
    [ requestGet('http://localhost:8084/emp/employers/'), requestGet('http://localhost:8084/case/cases/'), requestGet('http://localhost:8084/dept/departments/') ]).then(([ datas, delas, departs ]) =>
    this.setState({ datas, delas, departs}));

}

handleSelect(key) {
  console.log(`selected ${key}`);
this.setState({ key });

}

handleShow() {
  this.setState({ show: true });
}
handleShowDetail() {
  this.setState({ showdetail: true });
}


handleHide() {
  this.setState({ show: false, showdetail: false});
}

handleSelect(key) {
  console.log(`selected ${key}`);
this.setState({ key });

$.ajax({
     type: 'GET',
     url: `http://localhost:8084/emp/employers/${key}`,
     dataType: 'json',
     success: function(res) {



         console.log("id dep in emp " + res.dept_id)



         $.ajax({
              type: 'GET',
              url: `http://localhost:8084/dept/departments/${res.dept_id}`,
              dataType: 'json',
              success: function(res2) {


                  console.log("id emp " +  res.id)
                  console.log("id dep in emp in 2select " +res2.id)

                  $(`.Boo1-${res.id}`).html(res2.title);
                  $(`.Boo2-${res.id}`).html(res2.phone);
                  $(`.Boo3-${res.id}`).html(res2.spokesman);
                  $(`.Boo4-${res.id}`).html(res2.contact);




              }
            });

     }
   });

}





handleGetModal(key) {
   console.log("good")
   console.log(`принят в модал ${key}`);
   $.ajax({
        type: 'GET',
        url: `http://localhost:8084/emp/employers/${key}`,
        dataType: 'json',
        success: function(res) {



            console.log(res.id)
  console.log(res.name)
    console.log(res.rank)
      console.log(res.characteristic)
        console.log(res.dept_id)
        console.log(res.list_id)
          console.log(res.birthday)





             $('.sotr_id').val(res.id);
             $('.sotr_name').val(res.name);
             $('.sotr_rank').val(res.rank);
             $('.sotr_characteristic').val(res.characteristic);
             $('.sotr_dept_id').val(res.dept_id);
             $('.sotr_list_id').val(res.list_id);
             $('.sotr_birthday').val(res.birthday);

        }
      });
 }

UpdateData(key){
console.log(`отправлен ${key}`);
  $.ajax ({

       url:`http://localhost:8084/emp/employers/`,
      type: "PUT",
      data: JSON.stringify({
        id:$('.sotr_id').val(),
        name:$('.sotr_name').val(),
              rank:$('.sotr_rank').val(),
        characteristic:$('.sotr_characteristic').val(),
        dept_id:$('.sotr_dept_id').val(),
        list_id:$('.sotr_list_id').val(),
                birthday:$('.sotr_birthday').val()

      }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function(res) {

      }.bind(this)
  });
};


handleChange(event) {

  this.setState({id: event.target.id});
  this.setState({name: event.target.name});
    this.setState({rank: event.target.rank});
  this.setState({characteristic: event.target.characteristic});
  this.setState({dept_id: event.target.dept_id});
  this.setState({list_id: event.target.list_id});
  this.setState({birthday: event.target.birthday});

}

handleSubmit(event){

  event.preventDefault();
console.log(this.state.id)
 console.log(this.state.name)
 console.log(this.state.rank)
 console.log(this.state.characteristic)
 console.log(this.state.dept_id)
 console.log(this.state.list_id)
 console.log(this.state.birthday)

    this.setState({ show: false });
  $.ajax ({
       url:"http://localhost:8084/emp/employers/",
      type: "POST",
      data: JSON.stringify({

        id:"",
        name:this.state.name,
        rank:this.state.rank,
        characteristic:this.state.characteristic,
        dept_id:this.state.dept_id,
        list_id:this.state.list_id,
        birthday:this.state.birthday

      }),
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      success: function(res) {
          console.log(res);
          this.setState({ show: false });
          console.log("Added");
      }.bind(this)
  });
};


DropData(key) {

      console.log(`на удаление ${this.state.key}`);


    $.ajax ({

         url:`http://localhost:8084/emp/employers/${this.state.key}`,
        type: "DELETE",

        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(res) {

        }.bind(this)
    });

}

  render() {
    return (
      <div className="home">



      <Modal
        {...this.props}
        show={this.state.show}
        onHide={this.handleHide}
        dialogClassName="custom-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">
            Добавить сотрудника
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>

        <form onSubmit={this.handleSubmit}>




                        <FormGroup  className="hidden"  controlId="case_id_dela">
                          <ControlLabel>id сотрудника</ControlLabel>
                          <FormControl className="sotr_id" value={this.state.id} onChange={(ev)=>this.setState({id:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>

                        <FormGroup controlId="case_id_dela">
                          <ControlLabel>ФИО сотрудника</ControlLabel>
                          <FormControl className="sotr_name" value={this.state.name} onChange={(ev)=>this.setState({name:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>

                        <FormGroup controlId="case_id_dela">
                          <ControlLabel>Звание</ControlLabel>
                          <FormControl className="sotr_rank" value={this.state.rank} onChange={(ev)=>this.setState({rank:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>

                        <FormGroup controlId="case_id_dela">
                          <ControlLabel>Описательная характеристика</ControlLabel>
                          <FormControl className="sotr_characteristic" value={this.state.characteristic} onChange={(ev)=>this.setState({characteristic:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>

                        <FormGroup controlId="case_city">
                         <ControlLabel>Департамент</ControlLabel>
                         <FormControl onChange={(ev)=>this.setState({dept_id:ev.target.value})}  componentClass="select" placeholder="select">

                         <option value="0">Выбрать...</option>

                         {this.state.departs.map(function (item, key) {
                             return (
                                 <option value={item.id}>{item.title} </option>
                               )
                           })}


                         </FormControl>
                       </FormGroup>

                        <FormGroup controlId="case_city">
                         <ControlLabel>Закрепленное дело</ControlLabel>
                         <FormControl onChange={(ev)=>this.setState({list_id:ev.target.value})}  componentClass="select" placeholder="select">

                         <option value="0">Выбрать...</option>

                         {this.state.delas.map(function (item, key) {
                             return (
                                 <option value={item.id}>{item.name} </option>
                               )
                           })}


                         </FormControl>
                       </FormGroup>

                        <FormGroup controlId="case_id_dela">
                          <ControlLabel>день рождения</ControlLabel>
                          <FormControl className="sotr_birthday" value={this.state.birthday} onChange={(ev)=>this.setState({birthday:ev.target.value})} type="text" placeholder="" />
                        </FormGroup>







              </form>

        </Modal.Body>
        <Modal.Footer>
        <input className="btn btn-primary" type="submit" onClick={this.handleSubmit} value="Отправить" />



          <Button onClick={this.handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>



      <Modal
        {...this.props}
        show={this.state.showdetail}
        onHide={this.handleHide}
        onEntered={() => this.handleGetModal(this.state.key)}
        dialogClassName="custom-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">
            Управлять сотрудником
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>


                                <FormGroup  className="hidden"  controlId="case_id_dela">
                                  <ControlLabel>id сотрудника</ControlLabel>
                                  <FormControl className="sotr_id" value={this.state.id} onChange={(ev)=>this.setState({id:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>ФИО сотрудника</ControlLabel>
                                  <FormControl className="sotr_name" value={this.state.name} onChange={(ev)=>this.setState({name:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>Звание</ControlLabel>
                                  <FormControl className="sotr_rank" value={this.state.rank} onChange={(ev)=>this.setState({rank:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>Описательная характеристика</ControlLabel>
                                  <FormControl className="sotr_characteristic" value={this.state.characteristic} onChange={(ev)=>this.setState({characteristic:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>Номер департамента</ControlLabel>
                                  <FormControl className="sotr_dept_id" value={this.state.dept_id} onChange={(ev)=>this.setState({dept_id:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>







                                           <FormGroup controlId="case_city">
                                            <ControlLabel>Закрепленное дело</ControlLabel>
                                            <FormControl onChange={(ev)=>this.setState({list_id:ev.target.value})}  componentClass="select" placeholder="select">

                                            <option value="0">Выбрать...</option>

                                            {this.state.delas.map(function (item, key) {
                                                return (
                                                    <option value={item.id}>{item.name} </option>
                                                  )
                                              })}


                                            </FormControl>
                                          </FormGroup>

                                <FormGroup controlId="case_id_dela">
                                  <ControlLabel>день рождения</ControlLabel>
                                  <FormControl className="sotr_birthday" value={this.state.birthday} onChange={(ev)=>this.setState({birthday:ev.target.value})} type="text" placeholder="" />
                                </FormGroup>
        </Modal.Body>
        <Modal.Footer>
                            <input className="btn btn-primary" type="submit" onClick={this.UpdateData} value="Отправить" />
          <Button onClick={this.handleHide}>Close</Button>
        </Modal.Footer>
      </Modal>









      <Nav onSelect={key => this.handleSelect(key)}>
             <MenuItem className="roso2"  onClick={this.handleShow} eventKey={this.state.key}>Добавить сотрудника</MenuItem>
             <MenuItem className="roso2"  onClick={this.handleShowDetail} eventKey={this.state.key}>Управлять сотрудником</MenuItem>
             <MenuItem className="roso2"   onClick={this.DropData}  eventKey={this.state.key}>Удалить сотрудника</MenuItem>
           </Nav>











      <Tab.Container
      activeKey={this.state.key}
      onSelect={this.handleSelect}
      >
 <Row className="clearfix">
   <Col sm={12}>

   {this.state.datas.map(function (item, key) {
       return (



     <Nav bsStyle="pills" eventKey={item.id}stacked>


       <NavItem eventKey={item.id}>
       <p className="ppad">
       <Col xs="2" className="nopadding"><b>Номер: </ b> <span>{item.id}</span></Col>
       <Col xs="4" className="nopadding" ><b>ФИО: </ b> <span>{item.name}</span></Col>
      <Col xs="3" className="nopadding" ><b>Звание: </ b> <span>{item.rank}</span></Col>
       <Col xs="3" className="nopadding" ><b>Дата рождения: </ b><span>{item.birthday}</span></Col>

     </p>
       </NavItem>
       <Tab.Content>
         <Tab.Pane className="astab"  eventKey={item.id}>

         <li className="ppad2">

          <Col xs="5" className="nopadding">
              <Col xs="12" className="nopadding centered"><b>Отдел: </ b><span className={`Boo1-${item.id}`}>NaN</span></Col>
              <Col xs="12" className="nopadding"><b>Телефон: </ b><span className={`Boo2-${item.id}`}> NaN</span></Col>
              <Col xs="12" className="nopadding" ><b>Начальник: </ b><span className={`Boo3-${item.id}`}>NaN</span></Col>
              <Col xs="12" className="nopadding" ><b>Адрес: </ b><span className={`Boo4-${item.id}`}>NaN</span></Col>
          </Col>

          <Col xs="7" className="nopadding">
          <span>{item.characteristic}</span>
          </Col>



       </li>

         </Tab.Pane>
       </Tab.Content>









     </Nav>

   )
})}

   </Col>
   <Col sm={8}>

   </Col>
 </Row>
</Tab.Container>

      </div>
    );
  }
}

export default Sotr;
