<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%-- <%@ taglib prefix="comp" tagdir="/WEB-INF/tags"%> --%>
<%@ page contentType="text/html;charset=utf-8" %>




<%@page session="false" %>
<html>
<head>
  <meta charset="utf-8">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <title>Login Page</title>
    <style>

    body{
        background: #2f333c;
    }
        .error {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .msg {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

        #login-box {
          width: 300px;
  padding: 20px;
  margin: 100px auto;
  background: #ffffff;
  -webkit-border-radius: 3px;
  border-top: 5px solid #00c4ff;
  -moz-border-radius: 2px;
  border: 1px solid #dadada;
        }
    </style>
</head>
<body>

<div id="login-box">

    <h3 style="
    font-size: 15px;
    text-align: center;
    padding: -1px;
    margin-top: 7px;
    margin-bottom: 34px;
">Форма входа</h3>

    <c:if test="${not empty error}">
        <div class="error">${error}</div>
    </c:if>
    <c:if test="${not empty msg}">
        <div class="msg">${msg}</div>
    </c:if>

    <form onSubmit="return checkAnswer();" name='loginForm' action="<c:url value='/' />" method='POST'>

        <table>
            <tr>
   <i class="fas fa-user" style="
   position: absolute;
top: 193px;
left: 50%;
color: #dadada;
margin-left: -116px;
font-size: 17px;
"></i>
                <td>    <input id="answer" type="text" maxlength="55" class="box" style="
                background: white;
          padding: 13px 12px 13px 40px;
      outline: white;
      color: #000000;
      width: 253px;
      font-weight: 200;
      font-size: 17px;
      border-radius: 3px;
      border: 1px solid #dadada;
      margin-bottom: 14px;
" /></td>
            </tr>
            <tr>

        <i class="fas fa-unlock-alt" style="
    position: absolute;
    top: 255px;
    left: 50%;
    color: #dadada;
    margin-left: -116px;
    font-size: 17px;
"></i>


                <td><input id="answer23" type='password' name='password'  style="
                background: white;
            padding: 13px 12px 13px 40px;
        outline: white;
        color: #000000;
        width: 253px;
        font-weight: 200;
        font-size: 17px;
        border-radius: 3px;
        border: 1px solid #dadada;
        margin-bottom: 14px;
"/></td>
            </tr>
            <tr>
                <td colspan='2'><input name="submit" type="submit"   style="
    /* text-align: center; */
    background: #666f77;
    padding: 12px 24px;
    color: white;
    font-size: 14px;
    border-radius: 3px;
    margin-left: 77px;
    margin-top: 19px;
    margin-bottom: -14px;
    border: 0px;
    border-bottom: 4px solid #4c5965;
"/></td>
            </tr>
        </table>

        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}1Vs62hG"/>
        <c:url value="/logout" var="logoutUrl" />

    </form>

    <script>
    function checkAnswer(){
        var response = document.getElementById('answer23').value;  if (response == "111")   location = 'http://localhost:8084/main';  else
            location = 'http://localhost:8084/';
        return false;

    }
    </script>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
